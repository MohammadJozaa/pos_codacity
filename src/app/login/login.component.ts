import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public email="";

  constructor() { }

  ngOnInit() {
  }

  onChangeEmail(event){
    this.email = event.target.value;
    //console.log("E: ",this.email);
  }
  onClickTest(){
    console.log("you click this button");
    console.log(this.email);
  }

}
