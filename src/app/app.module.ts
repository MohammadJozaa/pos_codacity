import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AddTransactionComponent } from './add-transaction/add-transaction.component';
import { ShowTransactionComponent } from './show-transaction/show-transaction.component';
import { MainPageComponent } from './main-page/main-page.component';

const appRoutes: Routes = [
  { path: '',         component: MainPageComponent },
  { path: 'login',    component: LoginComponent },
  { path: 'show_transaction',     component: ShowTransactionComponent },
  { path: 'login/add_transaction',      component: AddTransactionComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AddTransactionComponent,
    ShowTransactionComponent,
    MainPageComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
